@php
    $nav_links = [
        [
            'name' => 'Home',
            'route' => route('home'),
            'active' => request()->routeIs('home')
],
        [
            'name' => 'Personas',
            'route' => route('personas.create'),
            'active' => false
]
];
@endphp
<nav x-data="{ open: false }" class="bg-white border-b border-gray-100 shadow">
    <!-- Primary Navigation Menu -->
    <div class="mx-auto px-4 sm:px-6 lg:px-8 h-20">
        <div class="grid grid-cols-12 gap-2">
            <div class="align-middle col-span-5 pt-5">
                <h2 class="text-blue-600 text-2xl md:text-4xl inline">Back Office FCI</h2>
            </div>
            <div class="col-span-7">
                <h1 class="w-min mx-auto text-4xl text-blue-600 pt-5 inline">
                    <a class="leading-loose h-1" href="{{route('home')}}">Agenda</a>
                </h1>
            </div>
        </div>
    </div>
    <!-- Responsive Navigation Menu -->
    <div :class="{'block': open, 'hidden': ! open}" class="hidden sm:hidden">

    </div>
</nav>
