<x-app-layout>
    <div class="py-0">
        <div class="mt-4 mb-6 items-center w-1/3 mx-auto mt-10">
            @include('partials.formSearch')
        </div>
        <!-- This is an example component -->
        <div class='flex items-center justify-center mt-0 mb-2'>
            <div class="rounded-xl border p-5 shadow-md w-3/5 bg-white">
                @foreach($personas as $persona)
                    @include('partials.mostrarPersona',[
                                'emails' => explode(";", $persona->correo),
                                'contactos' => explode(";", $persona->contacto),
                                'telefonos' => explode(";", $persona->telefono)])
                @endforeach
            </div>
        </div>
    </div>
    <div class="mr-96 mt-10">
        {{$personas->links()}}
    </div>
</x-app-layout>
