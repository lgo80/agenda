<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>
    <div class="py-0">
        <div class="mt-4 mb-6 items-center w-1/3 mx-auto mt-20">
            @include('partials.formSearch')
        </div>
        <!-- This is an example component -->
        <div class='flex items-center justify-center mt-20'>
            <div class="rounded-xl border p-5 shadow-md w-3/5 bg-white">
                @include('partials.mostrarPersona',['emails' => $emails, 'contactos' => $contactos,'telefonos' => $telefonos])
                <div>
                    <div class="flex items-center justify-between text-slate-500">
                        <div class="flex space-x-4 md:space-x-8">
                            <a class="flex cursor-pointer items-center transition hover:text-slate-600"
                               href="{{route('personas.edit',$persona)}}" title="Editar entidad">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24"
                                     stroke="currentColor" stroke-width="2">
                                    <path stroke-linecap="round" stroke-linejoin="round"
                                          d="M11 5H6a2 2 0 00-2 2v11a2 2 0 002 2h11a2 2 0 002-2v-5m-1.414-9.414a2 2 0 112.828 2.828L11.828 15H9v-2.828l8.586-8.586z"/>
                                </svg>
                            </a>
                            <form class="flex cursor-pointer items-center transition hover:text-slate-600"
                                  action="{{route('personas.destroy',$persona)}}" method="post">
                                @csrf
                                @method('delete')
                                <button type="submit" title="Eliminar entidad">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none"
                                         viewBox="0 0 24 24"
                                         stroke="currentColor" stroke-width="2">
                                        <path stroke-linecap="round" stroke-linejoin="round"
                                              d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"/>
                                    </svg>
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
