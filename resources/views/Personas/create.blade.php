<x-app-layout>
    <div class="mt-10 md:mt-16 md:col-span-2 w-2/3 mx-auto">
        @include('partials.form',['route' => route('personas.store'),
'editar' => false,'titulo' => 'Crear Entidad','nombreSubmit' => 'Grabar entidad'])
    </div>
</x-app-layout>
