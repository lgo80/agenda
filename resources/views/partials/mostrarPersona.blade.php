<div class="flex w-full items-center justify-between border-b pb-3">
    <div class="flex items-center space-x-3">
        <div class="text-lg font-bold text-slate-700">{{$persona->entidad}}</div>
    </div>
    <div class="flex items-center space-x-8">
        <div class="text-base text-neutral-500">Número de entidad: {{$persona->codigo}}</div>
    </div>
</div>
<div class="mt-4 mb-6 items-center border-b pb-3">
    <div class="ml-5 grid grid-cols-12 gap-2">
        <div class="col-span-12 xl:col-span-7 2xl:col-span-5 xl:border-r-4">
            <h4 class="underline text-base italic">Emails:</h4>
            <ul class="list-disc">
                @foreach($emails as $email)
                    @if($email)
                        <li>{{$email}}</li>
                    @endif
                @endforeach
            </ul>
        </div>
        <div class="col-span-12 xl:col-span-5 2xl:col-span-3 2xl:border-r-4 xl:mx-6">
            <h4 class="underline text-base italic">Contactos:</h4>
            <ul class="list-disc">
                @foreach($contactos as $contacto)
                    @if($contacto)
                        <li>{{$contacto}}</li>
                    @endif
                @endforeach
            </ul>
        </div>
        <div class="col-span-12 2xl:col-span-4">
            <h4 class="underline text-base italic">Teléfonos:</h4>
            <ul class="list-disc">
                @foreach($telefonos as $telefono)
                    @if($telefono)
                        <li>{{$telefono}}</li>
                    @endif
                @endforeach
            </ul>
        </div>
    </div>
</div>
