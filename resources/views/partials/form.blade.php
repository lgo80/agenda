<form action="{{$route}}" method="POST">
    @csrf
    @if($editar)
        @method('put')
    @endif
    @php
        $codigo = (isset($persona)) ? $persona->codigo : null;
        $entidad = (isset($persona)) ? $persona->entidad : null;
        $contacto = (isset($persona)) ? $persona->contacto : null;
        $telefono = (isset($persona)) ? $persona->telefono : null;
        $correo = (isset($persona)) ? $persona->correo : null;
    @endphp
    <div class="shadow overflow-hidden sm:rounded-md">
        <div class="px-4 py-5 bg-white sm:p-6">
            <div class="grid grid-cols-6 gap-6">
                <div class="col-span-12 sm:col-span-6 mx-auto text-4xl text-blue-600">
                    <h1>{{$titulo}}</h1>
                </div>
                <div class="col-span-6 sm:col-span-3">
                    <label for="codigo" class="block text-sm font-medium text-gray-700">Código de la
                        entidad:</label>
                    <input type="number" name="codigo" id="codigo" autocomplete="given-name"
                           placeholder="Ingresar código..." value="{{old('codigo',$codigo)}}"
                           class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                    @error('codigo')
                    <br/>
                    <small>*{{$message}}</small>
                    <br/>
                    @enderror
                </div>
                <div class="col-span-6 sm:col-span-3">
                    <label for="entidad" class="block text-sm font-medium text-gray-700">Entidad:</label>
                    <input type="text" name="entidad" id="entidad" autocomplete="family-name"
                           placeholder="Ingresar entidad..." value="{{old('entidad',$entidad)}}"
                           class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                    @error('entidad')
                    <br/>
                    <small>*{{$message}}</small>
                    <br/>
                    @enderror
                </div>
                <div class="col-span-4 sm:col-span-2">
                    <label for="contacto" class="block text-sm font-medium text-gray-700">Contactos:</label>
                    <div class="mt-1">
                                <textarea id="contacto" name="contacto" rows="3"
                                          class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 mt-1 block w-full sm:text-sm border border-gray-300 rounded-md"
                                          placeholder="Ingresar el/los contactos ...">{{old('contacto',$contacto)}}</textarea>
                    </div>
                    @error('contacto')
                    <br/>
                    <small>*{{$message}}</small>
                    <br/>
                    @enderror
                </div>

                <div class="col-span-4 sm:col-span-2">
                    <label for="telefono" class="block text-sm font-medium text-gray-700">Teléfonos:</label>
                    <div class="mt-1">
                                <textarea id="telefono" name="telefono" rows="3"
                                          class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 mt-1 block w-full sm:text-sm border border-gray-300 rounded-md"
                                          placeholder="Ingresar el/los teléfonos...">{{old('telefono',$telefono)}}</textarea>
                    </div>
                    @error('telefono')
                    <br/>
                    <small>*{{$message}}</small>
                    <br/>
                    @enderror
                </div>
                <div class="col-span-4 sm:col-span-2">
                    <label for="correo" class="block text-sm font-medium text-gray-700">Correos
                        electrónicos:</label>
                    <div class="mt-1">
                                <textarea id="correo" name="correo" rows="3"
                                          class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 mt-1 block w-full sm:text-sm border border-gray-300 rounded-md"
                                          placeholder="Ingresar el/los correos electrónicos...">{{old('correo',$correo)}}</textarea>
                    </div>
                    @error('correo')
                    <br/>
                    <small>*{{$message}}</small>
                    <br/>
                    @enderror
                </div>
            </div>
        </div>
        <div class="px-4 py-3 bg-gray-50 text-right sm:px-6">
            <button type="submit"
                    class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 transition ease-in-out delay-150 bg-blue-500 hover:-translate-y-1 hover:scale-110 hover:bg-indigo-500 duration-300">
                {{$nombreSubmit}}
            </button>
        </div>
    </div>
</form>
