<x-app-layout>
    <div class="py-80 bg-transparent">
        <div class="bg-transparent max-w-2xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-transparent overflow-hidden shadow-xl sm:rounded-lg">
                @include('partials.formSearch')
            </div>
        </div>
    </div>
</x-app-layout>
