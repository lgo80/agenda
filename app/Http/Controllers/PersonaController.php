<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePersona;
use App\Models\Persona;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class PersonaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index(): Application|Factory|View
    {
        $personas = Persona::paginate(5);
        return view('Personas/index', compact('personas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create(): Application|View|Factory
    {
        return view('Personas/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StorePersona $request
     * @return RedirectResponse
     */
    public function store(StorePersona $request): RedirectResponse
    {
        $persona = Persona::create($request->all());
        return redirect()->route('personas.show', $persona);
    }

    /**
     * Display the specified resource.
     *
     * @param Persona $persona
     * @return View|Factory|RedirectResponse|\Illuminate\Contracts\Foundation\Application
     */
    public function show(Persona $persona):
    View|Factory|RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        return view('Personas/show', [
            'persona' => $persona,
            'emails' => explode(";", $persona->correo),
            'contactos' => explode(";", $persona->contacto),
            'telefonos' => explode(";", $persona->telefono)
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @return View|Factory|RedirectResponse|\Illuminate\Contracts\Foundation\Application
     */
    public function search(Request $request):
    View|Factory|RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        $personaBuscada = Persona::where('entidad', 'like', '%' . $request->entidad . '%')
            ->take(1)->get();
        return (count($personaBuscada) == 0)
            ? redirect()->route('home')->with('status', 'La persona buscada no se encuentra')
            : redirect()->route('personas.show', $personaBuscada[0]->id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Persona $persona
     * @return Application|Factory|View
     */
    public function edit(Persona $persona): Application|Factory|View
    {
        return view('Personas/edit', compact('persona'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StorePersona $request
     * @param Persona $persona
     * @return RedirectResponse
     */
    public function update(StorePersona $request, Persona $persona): RedirectResponse
    {
        $persona->update($request->all());
        return redirect()->route('personas.show', $persona);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Persona $persona
     * @return RedirectResponse
     */
    public function destroy(Persona $persona): RedirectResponse
    {
        $persona->delete();
        return redirect()->route('personas.index', $persona);
    }
}
