<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePersona extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'codigo' => 'required',
            'entidad' => 'required|min:2|max:100',
        ];
    }


    public function attributes(): array
    {
        return [
            'nombre' => 'nombre de la persona',
        ];
    }

    public function messages(): array
    {
        return [
            'codigo.required' => 'Debe ingresar un número de código de entidad válido',
            'entidad.required' => 'Debe ingresar una entidad válida',
        ];
    }
}
